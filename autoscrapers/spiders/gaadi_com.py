# -*- coding: utf-8 -*-
import scrapy
from autoscrapers.items import Vehicle
import json
import re
import random

def clean_json(string):
    string = re.sub(",[ \t\r\n]+}", "}", string)
    string = re.sub(",[ \t\r\n]+\]", "]", string)

    return string

class CarsforsaleComSpider(scrapy.Spider):
    name = 'gaadi_com'
    allowed_domains = ['gaadi.com']
    # start = 28000000
    start = 365523
    finish = 1515071
    # finish = 1513987
    start_urls = ('https://www.gaadi.com/usedcars/details-{}'.format(id) for id in range(start, finish))
    # start_urls = ('https://www.gaadi.com/usedcars/details-{}'.format(random.randint(366000, 1515071)) for x in range(5000))

    def parse(self, response):
        if response.status == 404:
            yield None
        try:
            url = response.url
            id = url.split('-')[-1]
            image_urls = response.xpath('//li[@data-gscr-thumb]/@data-gscr-thumb').extract()
            image_urls = [x.replace('gallery', 'original') for x in image_urls]
            details = dict()

            scripts = response.xpath('//script/text()').extract()
            good_scripts = [x for x in scripts if 'dataLayer' in x and 'model_name' in x]
            if len(good_scripts) == 0:
                yield None
            good_script = good_scripts[0]
            good_script_json = good_script.split('=', 1)[1]
            good_script_json = good_script_json.split('//')[0].replace("'", '"')
            good_script_json = clean_json(good_script_json)
            # print(good_script_json)
            details['google_targeted'] = json.loads(good_script_json)[0]
            d = response.xpath('//ul[@class="list-inline"]/li/span')
            d = [x.xpath('./text()').extract_first() for x in d]
            first = dict()
            for name, value in zip(d[1::2], d[::2]):
                name = name.replace('Model', 'Year')
                first[name] = value
            details['first'] = first
            d = response.xpath('//ul[@id="specList"]//span/text()').extract()
            details['list'] = d
            values = response.xpath('//ul[@class="fetd-list"]/li')
            names = response.xpath('//ul[@class="fetd-list"]/li/span')
            values = [x.xpath('./text()').extract_first() for x in values]
            names = [x.xpath('./text()').extract_first() for x in names]
            second = dict()
            for name, value in zip(names, values):
                second[name] = value
            details['second'] = second
            year = details['google_targeted']['model_year']
            make = details['google_targeted']['oem_name']
            model = details['google_targeted']['model_name']
            if (not make) or len(make) == 0:
                make = 'na'
            if (not model) or len(model) == 0:
                model = 'na'

            yield Vehicle(spider_name='gaadi_com',
                          url=url,
                          id=id,
                          make=make,
                          model=model,
                          image_urls=image_urls,
                          details=details,
                          year=year
                         )
        except:
            yield None
