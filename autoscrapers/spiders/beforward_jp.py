# -*- coding: utf-8 -*-
import scrapy
import re
from autoscrapers.items import Vehicle
import random

class BeforwardJpSpider(scrapy.Spider):
    name = 'beforward_jp'
    allowed_domains = ['beforward.jp']
    start = 1
    finish = 810147
    # start_urls = ('https://www.beforward.jp/subaru/impreza-sportswagon/bf659657/id/{}'.format(id) for id in range(start, finish))
    start_urls = ['https://www.beforward.jp/subaru/impreza-sportswagon/bf659657/id/{}'.format(random.randint(1, 810147)) for _ in range(5000)]
    # start_urls = ['https://www.beforward.jp/subaru/impreza-sportswagon/bf659657/id/810147',
                  # 'https://www.beforward.jp/subaru/impreza-sportswagon/bf659657/id/1',
                  # 'https://www.beforward.jp/toyota/caldina/bf682537/id/808815',
                 # ]


    def parse(self, response):
        url = response.url
        id = url.split('/')[-1]
        # print(id)

        scripts = response.xpath('//script[@type="text/javascript"]/text()').extract()
        good_scripts = [script for script in scripts if 'maker:' in script]
        good_script = good_scripts[0]

        make = good_script.split('maker: ["')[1].split('"],')[0]
        model = good_script.split('model: ["')[1].split('"],')[0]
        year = 'not found'
        if (make == '') or (not make):
            make = 'na'
        if (model == '') or (not model):
            model = 'na'
        image_urls = ['https:' + x for x in response.xpath('//ul[@id="gallery"]//a[@href]/@href').extract()]

        #ctrl+f характерист
        details = {}
        list1 = [1, 2, 3, 4, 5, 6, 9, 12, 13, 14]
        # d_names = response.xpath('//table[@class="specification"]/tr[{}]/th')
        # d_values = response.xpath('//table[@class="specification"]/tr[{}]/td')
        # d_names = [response.xpath('//table[@class="specification"]/tr[{}]/th/text()'.format(str(x))).extract_first() for x in list1] + [response.xpath('//table[@class="specification"]/tr[{}]/th[2]/text()'.format(str(x))).extract_first() for x in [7, 8]]
        # d_values = [response.xpath('//table[@class="specification"]/tr[{}]/td/text()'.format(str(x))).extract_first() for x in list1] + [response.xpath('//table[@class="specification"]/tr[{}]/td[2]/text()'.format(str(x))).extract_first() for x in [7, 8]]
        d_names = []
        d_values = []
        for x in range(1, 15):
            if x == 10 or x == 11:
                continue
            elif x == 7 or x == 8:
                d_names.append(response.xpath('//table[@class="specification"]/tr[{}]/th[2]/text()'.format(str(x))).extract_first())
                d_values.append(response.xpath('//table[@class="specification"]/tr[{}]/td[2]/text()'.format(str(x))).extract_first())
            else:
                d_names.append(response.xpath('//table[@class="specification"]/tr[{}]/th[1]/text()'.format(str(x))).extract_first())
                d_values.append(response.xpath('//table[@class="specification"]/tr[{}]/td[1]/text()'.format(str(x))).extract_first())
                d_names.append(response.xpath('//table[@class="specification"]/tr[{}]/th[2]/text()'.format(str(x))).extract_first())
                d_values.append(response.xpath('//table[@class="specification"]/tr[{}]/td[2]/text()'.format(str(x))).extract_first())
        first = {}
        for name, value in zip(d_names, d_values):
            if name == 'Manufacture':
                # print(1)
                continue
            try:
                first[name.replace('.', '')] = value
            except:
                pass
        details['first'] = first
        list_true = response.xpath('//td[@class="attached_on"]/text()').extract()
        list_false = response.xpath('//td[@class="attached_off"]/text()').extract()
        details['true'] = list_true
        details['false'] = list_false
        # print('success')
        yield Vehicle(spider_name='beforward_jp',
                      url=url,
                      id=id,
                      make=make,
                      model=model,
                      image_urls=image_urls,
                      details=details,
                      year=year
                     )
