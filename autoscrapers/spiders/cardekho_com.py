# -*- coding: utf-8 -*-
import scrapy
from autoscrapers.items import Vehicle
import random

class CardekhoComSpider(scrapy.Spider):
    name = 'cardekho_com'
    allowed_domains = ['cardekho.com']
    start = 1
    finish = 1780138
    # start_urls = ('https://www.cardekho.com/used-car-details/used-BMW-3-Series-320d-cars-New-Delhi_{}.htm'.format(id) for id in range(start, finish))
    start_urls = ['https://www.cardekho.com/used-car-details/used-BMW-3-Series-320d-cars-New-Delhi_{}.htm'.format(random.randint(1, 1780138)) for x in range(5000)]
    # start_urls = ['https://www.cardekho.com/used-car-details/used-BMW-3-Series-320d-cars-New-Delhi_1780138.htm',
                  # 'https://www.cardekho.com/used-car-details/used-BMW-3-Series-320d-cars-New-Delhi_1673829.htm',
                  # 'https://www.cardekho.com/used-car-details/used-Hyundai-EON-Sportz-cars-Hyderabad_1773829.htm',
                  # 'https://www.cardekho.com/used-car-details/used-Hyundai-EON-Sportz-cars-Hyderabad_1.htm',
                 # ]


    def parse(self, response):
        url = response.url
        id = url.split('_')[-1].replace('.htm', '')

        make = response.xpath('//input[@name="oemName"]/@value').extract_first()
        model = response.xpath('//input[@name="orgModelName"]/@value').extract_first()
        year = response.xpath('//input[@id="modelYear"]/@value').extract_first()
        imagebox = response.xpath('//div[@class="imagebox"]')
        first_image = imagebox.xpath('.//img[@src]/@src').extract_first()
        rest_images = imagebox.xpath('.//img[@data-gsll-src]/@data-gsll-src').extract()
        image_urls = [first_image] + rest_images
        no_image = 'https://img.gaadicdn.com/images/new-images/blank-carphoto-BIG.gif'
        if first_image == no_image:
            image_urls = []

        details = {}
        d_names = response.xpath('//div[@class="detailshold"]/ul/li/div[@class="dttwo"]')
        d_values = response.xpath('//div[@class="detailshold"]/ul/li/div[@class="dttop"]')
        d_names = [x.xpath('./text()').extract_first() for x in d_names]
        d_values = [x.xpath('./text()').extract_first() for x in d_values]
        first = {}
        for name, value in zip(d_names, d_values):
            first[name.replace('Model', 'Year')] = value
        details['first'] = first
        d_names = response.xpath('//div[@id="specifications"]//div[@class="leftview"]')
        d_values = response.xpath('//div[@id="specifications"]//div[@class="rightview"]')
        d_names = [x.xpath('./text()').extract_first() for x in d_names]
        d_values = [x.xpath('./text()').extract_first() for x in d_values]
        second = {}
        for name, value in zip(d_names, d_values):
            second[name] = value
        details['second'] = second
        third = {}
        d = response.xpath('//div[@id="features"]/div/ul/li')
        for mystical_object_handled_with_xpath in d:
            if mystical_object_handled_with_xpath.xpath('./span/text()').extract_first() == 'Car Exterior Features':
                third['Car Exterior Features'] = mystical_object_handled_with_xpath.xpath('./ul//span/text()').extract()
            elif mystical_object_handled_with_xpath.xpath('./span/text()').extract_first() == 'Car Entertainment Features':
                third['Car Entertainment Features'] = mystical_object_handled_with_xpath.xpath('./ul//span/text()').extract()
            elif mystical_object_handled_with_xpath.xpath('./span/text()').extract_first() == 'Car Comfort Features':
                third['Car Comfort Features'] = mystical_object_handled_with_xpath.xpath('./ul//span/text()').extract()
            elif mystical_object_handled_with_xpath.xpath('./span/text()').extract_first() == 'Car Interior Features':
                third['Car Interior Features'] = mystical_object_handled_with_xpath.xpath('./ul//span/text()').extract()
            elif mystical_object_handled_with_xpath.xpath('./span/text()').extract_first() == 'Car Safety Features':
                third['Car Safety Features'] = mystical_object_handled_with_xpath.xpath('./ul//span/text()').extract()
        details['third'] = third
        yield Vehicle(spider_name='cardekho_com',
                      url=url,
                      id=id,
                      make=make,
                      model=model,
                      image_urls=image_urls,
                      details=details,
                      year=year
                     )
