# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import hashlib
# from scrapy.contrib.pipeline.images import ImagesPipeline
from scrapy.contrib.pipeline.files import FilesPipeline
from scrapy.utils.project import get_project_settings
from scrapy.http import Request
import pickle
import os
import csv
from datetime import datetime

class AutoscrapersPipeline(object):
    def process_item(self, item, spider):
        return item

class NameFixerPipeline(object):
    def process_item(self, item, spider):
        item['model'] = item['model'].replace('*', ' ').replace('|', ' ')\
                    .replace('\\', ' ').replace(':', ' ').replace('"', ' ')\
                    .replace('/', ' ').replace('?', ' ').strip()
        item['make'] = item['make'].replace('*', ' ').replace('|', ' ')\
                    .replace('\\', ' ').replace(':', ' ').replace('"', ' ')\
                    .replace('/', ' ').replace('?', ' ').strip()

        return item


class StoreImgPipeline(FilesPipeline):

    def get_media_requests(self, item, info):
        for nr, image_url in enumerate(item['image_urls']):
           yield Request(image_url, meta={'item': item, 'nr': nr})

    def file_path(self, request, response=None, info=None):
        vehicle = request.meta['item']
        nr = request.meta['nr']
        return '%s/%s/%s/%s/%s.jpg' % (vehicle['spider_name'], vehicle['make'], vehicle['model'], vehicle['id'], str(nr))


class TextFilePipeline(object):
    def save_text(self, vehicle, name):
        settings = get_project_settings()
        vehicle_data_dir = os.path.join(settings['FILES_STORE'], name, vehicle['make'], vehicle['model'], vehicle['id'])
        if not os.path.exists(vehicle_data_dir):
            os.makedirs(vehicle_data_dir)
        vehicle_data_file_path = os.path.join(vehicle_data_dir, 'data.txt')
        with open(vehicle_data_file_path, 'w') as vehicle_data_file:
            vehicle_data_file.write(str(dict(vehicle)))

    def process_item(self, item, spider):
        self.save_text(item, spider.name)
        return item
