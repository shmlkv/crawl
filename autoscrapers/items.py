# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class Vehicle(scrapy.Item):
    spider_name = scrapy.Field()
    url = scrapy.Field()
    image_urls = scrapy.Field()
    id = scrapy.Field()
    make = scrapy.Field()
    model = scrapy.Field()
    details = scrapy.Field()
    year = scrapy.Field()
