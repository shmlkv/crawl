# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy.exceptions import IgnoreRequest
from scrapy import signals
from autoscrapers.items import Vehicle
import logging
from scrapy.utils.project import get_project_settings

import stem
import stem.connection
from stem import Signal
from stem.control import Controller

from xml.dom import minidom
import random


class AutoscrapersSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        if response.status in [404, 410, 403]:

            raise Exception('YAY')
        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        file_name = str(spider.name) + '_errors.txt'
        with open(file_name, 'a') as errors:
            errors.write(str(response.status) + ' ==>> ' + str(response.url) + '\n')
        if response.status in [404, 410]:
            return (None, None)
        if response.status == 403:
            print(str(response.status) + ' We got banned')
            return(None, None)
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        return None

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)
